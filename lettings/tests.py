import pytest

from django.urls import reverse

from lettings.models import Address, Letting


@pytest.mark.django_db
def test_can_access_homepage(client):
    response = client.get(reverse('lettings:index'))
    assert response.status_code == 200, f"Lettings homepage status code is {response.status_code}"


@pytest.mark.django_db
def test_homepage_as_title(client):
    response = client.get(reverse('lettings:index'))
    assert f'<h1>Lettings</h1>' in str(response.content), f"Homepage doesn't contains title"


@pytest.mark.django_db
def test_index_page_include_all_links_to_details(client):
    Address.objects.bulk_create([
        Address(number=1, street="street", city="Heaven", state="Heaven", zip_code="00001",
                country_iso_code="01"),
        Address(number=1, street="street", city="Heaven", state="Heaven", zip_code="00001",
                country_iso_code="01"),
    ])
    address = Address.objects.all()
    Letting.objects.bulk_create([
        Letting(address=address[0], title="Underground Hygge"),
        Letting(address=address[1], title="Pirate of the Caribbean Getaway"),
    ])

    response = client.get(reverse('lettings:index'))
    detail_urls = [
        reverse('lettings:show', args=[letting.id]) for letting in Letting.objects.all()
    ]
    for url in detail_urls:
        assert f'href="{url}"' in str(
            response.content), f"Link to {url} not found on lettings homepage"


@pytest.mark.django_db
def test_can_access_letting_detail_with_title(client):
    address = Address(number=1, street="street", city="Heaven", state="Heaven", zip_code="00001",
                      country_iso_code="001")
    address.save()
    letting = Letting(address=address, title="Best BNB")
    letting.save()
    response = client.get(reverse('lettings:show', args=[letting.id]))
    assert response.status_code == 200, f"Cannot access to letting detail for {letting}"
    assert f"<h1>{letting.title}</h1>" in str(response.content)
