from django.urls import path
from .views import index, show

app_name = 'lettings'

urlpatterns = [
    path('', index, name='index'),
    path('<int:letting_id>/', show, name='show'),
]
