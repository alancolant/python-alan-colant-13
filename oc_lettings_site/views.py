from django.shortcuts import render


def index(request):
    return render(request, 'index.html')


def sentry(request):
    raise Exception("Error2")
