from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static

from . import views, settings

urlpatterns = [
    path('', views.index, name='index'),
    path('sentry-debug/', views.sentry),
    path('lettings/', include('lettings.urls', namespace="lettings")),
    path('profiles/', include('profiles.urls', namespace="profiles")),
    path('admin/', admin.site.urls),
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
