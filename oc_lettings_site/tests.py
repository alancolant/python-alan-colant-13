from django.urls import reverse


def test_can_access_homepage(client):
    response = client.get(reverse('index'))
    assert response.status_code == 200, f"Homepage status code is {response.status_code}"


def test_homepage_as_title(client):
    response = client.get(reverse('index'))
    assert f'Welcome to Holiday Homes' in str(response.content), f"Homepage doesn't contains title"


def test_homepage_includes_profiles_and_lettings_links(client):
    response = client.get(reverse('index'))
    for url in [reverse('lettings:index'), reverse('profiles:index')]:
        assert f'href="{url}"' in str(response.content), f"Link to {url} not found on homepage"
