from django.contrib.auth.models import User
import pytest

from django.urls import reverse

from profiles.models import Profile


@pytest.mark.django_db
def test_can_access_profiles_index(client):
    response = client.get(reverse('profiles:index'))
    assert response.status_code == 200, f"Profiles homepage status code is {response.status_code}"


@pytest.mark.django_db
def test_profiles_index_as_title(client):
    response = client.get(reverse('profiles:index'))
    assert f'<h1>Profiles</h1>' in str(response.content), f"Profiles index doesn't contains title"


@pytest.mark.django_db
def test_index_page_include_all_links_to_details(client):
    User.objects.bulk_create([User(username="test"), User(username="test2")])
    users = User.objects.all()
    Profile.objects.bulk_create([
        Profile(user=users[0], favorite_city="Paris"),
        Profile(user=users[1], favorite_city="London"),
    ])

    response = client.get(reverse('profiles:index'))
    detail_urls = [reverse('profiles:show', args=[profile.user.username]) for profile in
                   Profile.objects.all()]
    for url in detail_urls:
        assert f'href="{url}"' in str(
            response.content), f"Link to {url} not found on profiles homepage"


@pytest.mark.django_db
def test_can_access_profile_detail_with_titles(client):
    user = User(username="test")
    user.save()
    profile = Profile(user=user, favorite_city="New York")
    profile.save()
    response = client.get(reverse('profiles:show', args=[profile.user.username]))
    assert response.status_code == 200, f"Cannot access to profile detail for {profile.user}"
    assert f"<h1>{user.username}</h1>" in str(response.content), \
        "Title doesn't appear in profile detail"
