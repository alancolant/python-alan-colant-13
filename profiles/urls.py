from django.urls import path
from .views import index, show

app_name = 'profiles'

urlpatterns = [
    path('', index, name='index'),
    path('<str:username>/', show, name='show'),
]
